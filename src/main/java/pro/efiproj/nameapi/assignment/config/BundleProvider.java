package pro.efiproj.nameapi.assignment.config;

import java.util.ResourceBundle;

public class BundleProvider {

    /**
     * Finds resource bundles by given name
     *
     * @param bundleName name of bundle to find
     * @return resource bundle found by given name
     */
    public static ResourceBundle findBundle(String bundleName) {
        return ResourceBundle.getBundle(bundleName);
    }
}
