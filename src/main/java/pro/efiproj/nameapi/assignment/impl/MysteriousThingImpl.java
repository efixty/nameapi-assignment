package pro.efiproj.nameapi.assignment.impl;

import pro.efiproj.nameapi.assignment.MysteriousThing;
import pro.efiproj.nameapi.assignment.config.BundleProvider;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ResourceBundle;

/**
 * This class is a thread-safe lazy load reflection-proof implementation of singleton pattern.
 * It provides implementation of methods defined in {@link MysteriousThing}.
 * This class stores maximum, minimum, count and total sum of all numbers added (total sum
 * and count are needed for average calculation).
 *
 * @see MysteriousThingImpl#getMysteriousThing() for obtaining an instance
 */
public final class MysteriousThingImpl implements MysteriousThing {
    private static MysteriousThingImpl MYSTERIOUS_THING;

    /**
     * As some methods, which require no synchronization, may be added
     * to this class in future, it uses a lock (dummy Object) in synchronized blocks.
     */
    private final Object lock = new Object();
    private final ResourceBundle messages = BundleProvider.findBundle("error_messages");

    /**
     * Maximum of input numbers
     */
    private BigDecimal max = BigDecimal.ZERO;

    /**
     * Minimum of input numbers
     */
    private BigDecimal min = BigDecimal.ZERO;

    /**
     * Count of input numbers
     */
    private BigDecimal counter = BigDecimal.ZERO;

    /**
     * Total sum of input numbers
     */
    private BigDecimal sum = BigDecimal.ZERO;

    /**
     * Private constructor which throws exception if the only
     * instance is already initialized.
     */
    private MysteriousThingImpl() {
        if (MYSTERIOUS_THING != null) {
            final String errorMsg = messages.getString("err.already.initialized");
            throw new IllegalStateException(errorMsg);
        }
    }

    /**
     * Method for obtaining an instance of this class.
     * Uses double check technique to provide thread safety and lazy initialization
     *
     * @return the only instance of this class
     */
    public static MysteriousThingImpl getMysteriousThing() {
        if (MYSTERIOUS_THING == null) {
            synchronized (MysteriousThingImpl.class) {
                if (MYSTERIOUS_THING == null) {
                    MYSTERIOUS_THING = new MysteriousThingImpl();
                }
            }
        }
        return MYSTERIOUS_THING;
    }

    /**
     * Invalidates current state.
     * Sets all fields to their default values.
     */
    public static void reset() {
        synchronized (MysteriousThingImpl.class) {
            MYSTERIOUS_THING = null;
            MYSTERIOUS_THING = new MysteriousThingImpl();
        }
    }

    @Override
    public void addNumber(int number) {
        addNumber(new BigDecimal(number));
    }

    @Override
    public void addNumber(long number) {
        addNumber(new BigDecimal(number));
    }

    @Override
    public void addNumber(double number) {
        addNumber(new BigDecimal(number));
    }

    @Override
    public void addNumber(Number number) {
        validateInput(number);
        addNumber(new BigDecimal(number.toString()));
    }

    @Override
    public void addNumber(String numberString) {
        validateNumericString(numberString);
        addNumber(new BigDecimal(numberString));
    }

    /**
     * Adds a number. If that number is a valid input:
     * 1) if it is bigger than maximum, then it becomes new maximum
     * 2) if it is smaller than minimum, then it becomes new minimum
     * Counter is incremented and input is added to total sum (for further average calculation)
     *
     * @param number to be added
     */
    @Override
    public void addNumber(BigDecimal number) {
        validateInput(number);
        synchronized (lock) {
            if (BigDecimal.ZERO.equals(counter)) {
                max = number;
                min = number;
            } else if (max.compareTo(number) < 0) {
                max = number;
            } else if (min.compareTo(number) > 0) {
                min = number;
            }
            sum = sum.add(number);
            counter = counter.add(BigDecimal.ONE);
        }
    }

    /**
     * Calculates average of all input numbers by dividing their total sum by their count
     *
     * @return result of division with maximum of 10 digits after comma
     */
    @Override
    public BigDecimal getAverage() {
        synchronized (lock) {
            validateState();
            return sum.divide(counter, 10, RoundingMode.HALF_EVEN).stripTrailingZeros();
        }
    }

    @Override
    public BigDecimal getMax() {
        synchronized (lock) {
            validateState();
            return max;
        }
    }

    @Override
    public BigDecimal getMin() {
        synchronized (lock) {
            validateState();
            return min;
        }
    }

    /**
     * Validates input to be a non-null object
     *
     * @param number input to be validated
     */
    private void validateInput(Object number) {
        if (number == null) {
            final String errorMsg = messages.getString("err.null.value");
            throw new IllegalArgumentException(errorMsg);
        }
    }

    /**
     * Validates state of this object, so that no info is provided unless at least one number is added
     */
    private void validateState() {
        if (counter.equals(BigDecimal.ZERO)) {
            final String errorMsg = messages.getString("err.no.numbers");
            throw new IllegalStateException(errorMsg);
        }
    }

    /**
     * Validates input to be a number
     * Splits the number by decimal point and then validates each part
     * If input's sign explicitly stated, then the sign is discarded during validation
     *
     * @param numericString input to be validated
     */
    private void validateNumericString(String numericString) {
        // check if input is null or empty
        if (numericString == null || numericString.trim().isEmpty()) throwNFE();

        // if input is negative we must validate only positive part
        if (numericString.charAt(0) == '-' || numericString.charAt(0) == '+') {
            numericString = numericString.substring(1);
        }

        // validates input not to start or end with a dot
        if (numericString.charAt(0) == '.' || numericString.charAt(numericString.length() - 1) == '.') throwNFE();

        // splitting input into decimal and floating point parts, then validating one by one
        String[] numericStringArr = numericString.split("[.]");

        validateWholePart(numericStringArr[0]);
        if (numericStringArr.length > 1) {
            // validates input not to contain more than one dot
            if (numericStringArr.length > 2) {
                throwNFE();
            }
            validateDecimalPart(numericStringArr[1]);
        }
    }

    /**
     * Validates whole part of the number (e.g. left of decimal point)
     * Checks the part not to start with 0 if it is not 0. For example: 012 is not valid.
     * Checks the part to consist of only digits
     *
     * @param wholePart input to be validated
     */
    private void validateWholePart(String wholePart) {
        if (wholePart.length() > 1 && wholePart.startsWith("0")) throwNFE();
        boolean isNumeric = isNumeric(wholePart);
        if (!isNumeric) throwNFE();
    }

    /**
     * Validates decimal part of the number (e.g. right of decimal point)
     * Checks the part to consist of only digits
     *
     * @param floatingPointPart input to be validated
     */
    private void validateDecimalPart(String floatingPointPart) {
        boolean isNumeric = isNumeric(floatingPointPart);
        if (!isNumeric) throwNFE();
    }

    /**
     * Checks if input string consists of only digits
     *
     * @param numericString input to be checked
     * @return true if input is a numeric string, false otherwise
     */
    private boolean isNumeric(String numericString) {
        return numericString.chars()
                .mapToObj(this::isDigit)
                .reduce(Boolean::logicalAnd)
                .orElse(false);
    }

    private boolean isDigit(int charCode) {
        return charCode >= '0' && charCode <= '9';
    }

    private void throwNFE() {
        final String errorMsg = messages.getString("err.badly.formatted");
        throw new NumberFormatException(errorMsg);
    }
}
