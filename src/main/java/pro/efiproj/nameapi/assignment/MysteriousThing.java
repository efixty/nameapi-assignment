package pro.efiproj.nameapi.assignment;

import java.math.BigDecimal;

/**
 * Class providing interface for given task
 * It has (overloaded) methods for adding a number for a novice programmer to work easily
 * It provides methods for getting maximum, minimum and average at any given time
 */
public interface MysteriousThing {

    /**
     * Overloaded method for adding number as primitive int.
     *
     * @param number primitive int
     */
    void addNumber(int number);

    /**
     * Overloaded method for adding number as primitive long.
     *
     * @param number primitive long
     */
    void addNumber(long number);

    /**
     * Overloaded method for adding number as primitive double.
     *
     * @param number primitive double
     */
    void addNumber(double number);

    /**
     * Overloaded method for adding number as a wrapper class.
     *
     * @param number wrapper class
     */
    void addNumber(Number number);

    /**
     * Overloaded method for adding number as string.
     * As double type stores numbers not in the exact form they were input,
     * floating point numbers should be added as strings and then be converted to
     * BigDecimal
     * Example: {@code new BigDecimal(0.0001).equals(new BigDecimal("0.0001"))} states {@code false}
     * Double is good at until a certain precision
     *
     * @param numberString numeric string of input number
     */
    void addNumber(String numberString);

    /**
     * Method for adding a number as BigDecimal
     *
     * @param number of BigDecimal type
     */
    void addNumber(BigDecimal number);

    /**
     * Method for getting average of all input numbers
     *
     * @return BigDecimal average of all input numbers
     */
    BigDecimal getAverage();

    /**
     * Method for getting maximum of input numbers
     *
     * @return maximum of input numbers
     */
    BigDecimal getMax();

    /**
     * Method for getting minimum of input numbers
     *
     * @return minimum of input numbers
     */
    BigDecimal getMin();
}
