package pro.efiproj.nameapi.assignment;

import org.hamcrest.CustomTypeSafeMatcher;
import org.junit.*;
import org.junit.rules.ExpectedException;
import pro.efiproj.nameapi.assignment.impl.MysteriousThingImpl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MysteriousThingBasicFlowTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();
    private MysteriousThing mysteriousThing;

    @Before
    public void setup() {
        mysteriousThing = MysteriousThingImpl.getMysteriousThing();
    }

    @Test(expected = IllegalStateException.class)
    public void noNumberAddedFlowTest() {
        getInfo();
    }

    @Test
    public void addPrimitiveIntTest() {
        final int input = 5;

        mysteriousThing.addNumber(input);

        final BigDecimal result = mysteriousThing.getMax();
        final BigDecimal expected = new BigDecimal(input);
        Assert.assertEquals(expected, result);
    }

    /**
     * This method tests the case when first number is added.
     * In that situation it must be the maximum, minimum and the average.
     */
    @Test
    public void firstAddTest() {
        final int input = 5;

        mysteriousThing.addNumber(input);

        final BigDecimal expected = new BigDecimal(input);
        Assert.assertEquals(expected, mysteriousThing.getMax());
        Assert.assertEquals(expected, mysteriousThing.getMin());
        Assert.assertEquals(expected, mysteriousThing.getAverage());
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullNumberAddTest() {
        final Number input = null;
        mysteriousThing.addNumber(input);

        getInfo();
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullStringAddTest() {
        final String input = null;
        mysteriousThing.addNumber(input);

        getInfo();
    }

    @Test(expected = NumberFormatException.class)
    public void badlyFormattedNumberStringTest() {
        final String input = "0514.01.";
        mysteriousThing.addNumber(input);

        getInfo();
    }

    @Test(expected = NumberFormatException.class)
    public void nonNumericStringAdd() {
        final String input = "bla bla";
        mysteriousThing.addNumber(input);

        getInfo();
    }

    /**
     * This method tests the case when an 'evil' programmer tries to obtain second instance
     * of this singleton class through reflection. An {@code IllegalStateException} must be thrown
     * in that case.
     * Since any exception during reflectively-invoked constructor call is wrapped in declared exception,
     * it should not be rethrown (as it is done for rest of exceptions in catch block). If we get an
     * {@code InvocationTargetException}, we must check the root cause, and if it is not {@code IllegalStateException},
     * the test case must be considered as failed. Used a custom matcher.
     *
     * @throws InvocationTargetException if there was exception during invocation
     * @see MysteriousThingBasicFlowTest#getCustomMatcher(String, Class)
     * @see MysteriousThingImpl#MysteriousThingImpl()
     */
    @Test
    public void anotherInstanceCreationTest() throws InvocationTargetException {
        final String causeDescription = "of instantiating singleton again";
        CustomTypeSafeMatcher<? extends Throwable> customMatcher =
                getCustomMatcher(causeDescription, IllegalStateException.class);

        exception.expect(InvocationTargetException.class);
        exception.expectCause(customMatcher);

        try {
            Class<MysteriousThingImpl> clazz = MysteriousThingImpl.class;
            Constructor<MysteriousThingImpl> privateConstructor = clazz.getDeclaredConstructor();
            privateConstructor.setAccessible(true);
            MysteriousThing anotherThing = privateConstructor.newInstance();
        } catch (InstantiationException | NoSuchMethodException | IllegalAccessException e) {
            // just rethrowing as runtime exception, no need to declare in method signature
            throw new RuntimeException(e);
        }
    }

    /**
     * This method tests the case when two threads simultaneously add number.
     * That scenario is hard to recreate, but part of the code, containing potential critical section,
     * is wrapped in synchronized block. To ensure that no number has been overwritten
     * (e.g. no context switch occurred during a thread's number addition and control handled to
     * the other thread in critical section), this method counts the average for added numbers
     * and then compares with the result we got in the 'thing'.
     *
     * @see MysteriousThingImpl#addNumber(BigDecimal)
     */
    @Test
    public void multipleThreadFirstAdd() {
        final Runnable addFirstNumber = () -> mysteriousThing.addNumber(Thread.currentThread().getName());

        final int numberOfThreads = 15;
        List<Thread> startedThreads = new ArrayList<>(numberOfThreads);
        for (int i = 0; i < numberOfThreads; i++) {
            Thread thread = new Thread(addFirstNumber, String.valueOf(i));
            startedThreads.add(thread);
            thread.start();
        }

        for (Thread thread : startedThreads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        final int resultValue = (0 + 14) / 2; // sum of arithmetic sequence divided by number of elements involved
        final BigDecimal expectedAverage = new BigDecimal(resultValue);
        final BigDecimal result = mysteriousThing.getAverage();
        Assert.assertEquals(expectedAverage, result);
    }

    /**
     * Adds primitive long to the 'thing'.
     * After 5 being added, 0 is the new minimum.
     */
    @Test
    public void addPrimitiveLongTest() {
        final int input1 = 5;
        final long input2 = 0L;

        mysteriousThing.addNumber(input1);
        mysteriousThing.addNumber(input2);

        final BigDecimal result = mysteriousThing.getMin();
        final BigDecimal expected = new BigDecimal(input2);
        Assert.assertEquals(expected, result);
    }

    /**
     * Adds primitive double to the 'thing'.
     * After 0 being added, 5.5 is the new maximum.
     */
    @Test
    public void addPrimitiveDoubleTest() {
        final int input1 = 0;
        final double input2 = 5.5;

        mysteriousThing.addNumber(input1);
        mysteriousThing.addNumber(input2);

        final BigDecimal result = mysteriousThing.getMax();
        final BigDecimal expected = new BigDecimal(input2);
        Assert.assertEquals(expected, result);
    }

    /**
     * Adds instance of {@code Number} to the 'thing'.
     * After {@code Double.MAX_VALUE} being added, {@code Long.MAX_VALUE} is the new maximum.
     */
    @Test
    public void addNumberTest() {
        final Number input1 = Long.MAX_VALUE;
        final Number input2 = Double.MAX_VALUE;

        mysteriousThing.addNumber(input1);
        mysteriousThing.addNumber(input2);

        final BigDecimal result = mysteriousThing.getMin();
        final BigDecimal expected = new BigDecimal(input1.toString());
        Assert.assertEquals(expected, result);
    }

    /**
     * Adds a number's string representation to the 'thing' .
     * After 0.2228301 being added, 0.22283 is the new minimum.
     */
    @Test
    public void addStringNumberTest() {
        final String input1 = "0.2228301";
        final String input2 = "0.22283";

        mysteriousThing.addNumber(input1);
        mysteriousThing.addNumber(input2);

        final BigDecimal result = mysteriousThing.getMin();
        final BigDecimal expected = new BigDecimal(input2);
        Assert.assertEquals(expected, result);
    }

    @Test
    public void addBigDecimalTest() {
        final BigDecimal input1 = BigDecimal.ONE;
        final BigDecimal input2 = BigDecimal.TEN;

        mysteriousThing.addNumber(input1);
        mysteriousThing.addNumber(input2);

        final BigDecimal result = mysteriousThing.getMax();
        Assert.assertEquals(input2, result);
    }

    @After
    public void resetThing() {
        MysteriousThingImpl.reset();
    }

    /**
     * Gets info about the state of 'thing'.
     * Numbers are converted to strings as double is not eligible here.
     *
     * @return string, carrying information about "thing's" state
     */
    private String getInfo() {
        return String.format(
                "Maximum is %s\nMinimum is %s\nAverage is %s\n",
                mysteriousThing.getMax(),
                mysteriousThing.getMax(),
                mysteriousThing.getAverage()
        );
    }

    /**
     * Creates a custom matcher of root cause of exception
     *
     * @param causeDescription description of root cause
     * @param rootCause        class of root cause
     * @param <T>              type of root cause
     * @return custom matcher for exceptions with root cause of {@code T}
     */
    private <T extends Throwable> CustomTypeSafeMatcher<Throwable> getCustomMatcher(String causeDescription, Class<T> rootCause) {
        if (rootCause == null) {
            throw new IllegalArgumentException("Exception class cannot be null");
        }

        return new CustomTypeSafeMatcher<Throwable>(causeDescription) {
            @Override
            protected boolean matchesSafely(Throwable cause) {
                return rootCause.getCanonicalName().equals(cause.getClass().getCanonicalName());
            }
        };
    }
}