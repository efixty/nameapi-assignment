package pro.efiproj.nameapi.assignment;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pro.efiproj.nameapi.assignment.impl.MysteriousThingImpl;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MysteriousThingCalculationTest {

    private MysteriousThing mysteriousThing;

    @Before
    public void setup() {
        mysteriousThing = MysteriousThingImpl.getMysteriousThing();
    }

    /**
     * Tests the maximum calculation functionality
     */
    @Test
    public void maximumCalculationTest() {
        int input1 = 0;
        int input2 = Integer.MAX_VALUE;

        mysteriousThing.addNumber(input1);
        mysteriousThing.addNumber(input2);

        BigDecimal result = mysteriousThing.getMax();
        BigDecimal expected = new BigDecimal(input2);
        Assert.assertEquals(expected, result);
    }

    /**
     * Tests the minimum calculation functionality
     */
    @Test
    public void minimumCalculationTest() {
        int input1 = 5;
        int input2 = Integer.MIN_VALUE;
        long input3 = Long.MIN_VALUE;

        mysteriousThing.addNumber(input1);
        mysteriousThing.addNumber(input2);
        mysteriousThing.addNumber(input3);

        BigDecimal result = mysteriousThing.getMin();
        BigDecimal expected = new BigDecimal(input3);
        Assert.assertEquals(expected, result);
    }

    /**
     * Tests the average calculation functionality
     */
    @Test
    public void averageCalculationTest() {
        int input1 = 15;
        String input2 = "0.333";
        long input3 = -9;

        mysteriousThing.addNumber(input1);
        mysteriousThing.addNumber(input2);
        mysteriousThing.addNumber(input3);

        BigDecimal result = mysteriousThing.getAverage();
        BigDecimal expected = new BigDecimal(input1)
                .add(new BigDecimal(input2))
                .add(new BigDecimal(input3))
                .divide(new BigDecimal(3), 10, RoundingMode.HALF_EVEN)
                .stripTrailingZeros();

        Assert.assertEquals(expected, result);
    }

    @After
    public void resetThing() {
        MysteriousThingImpl.reset();
    }
}
